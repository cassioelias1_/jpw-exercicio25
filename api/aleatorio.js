/*
    Lista um pokémon aleatorio da lista
*/

var data = require('../data')

module.exports = function(req, res){
    res.json(data.pokemon[Math.floor(Math.random() * data.pokemon.length)]);
}